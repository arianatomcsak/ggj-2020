﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Asteroid : MonoBehaviour
{
    [SerializeField] float movementSpeed;
    [SerializeField] float secondsAliveOffScreen;
    private Vector2 movementDir, initialPos;
    Animator anim;
    [SerializeField] GameObject deathEffect;
    private GameObject cameraObj;
    private bool inCameraView;
    private Vector2 gameSpaceBoundary;
    private GameObject playerA, playerB;
    [SerializeField] private float minDistToChase;
    private GameObject chasingTarget = null;
    private bool dying;

    private void Start()
    {
        //Refferences
        cameraObj = Camera.main.transform.gameObject;
        anim = GetComponent<Animator>();
        gameSpaceBoundary = Reference_Manager.gameSpaceSize;
        playerA = Reference_Manager.playerA;
        playerB = Reference_Manager.playerB;

        //Get Movement direction
        Vector2 targetPos = cameraObj.GetComponent<GetCenterPoint_Component>().
            GetCenterPoint(Reference_Manager.playerA.transform.position, Reference_Manager.playerB.transform.position);
        Vector2 dirOffset = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized * (2 + (cameraObj.GetComponent<Camera>().orthographicSize / 2));
        Vector2 dirToTarget = (targetPos + dirOffset - (Vector2)transform.position).normalized;
        initialPos = transform.position;
        movementDir = dirToTarget;
    }

    private void Update()
    {

        //Movement
        transform.Translate(movementDir.normalized * movementSpeed * Time.deltaTime);
        if (chasingTarget == null)
        {
            if (minDistFromObjectReached(playerA, minDistToChase))
            { chasingTarget = playerA;
                if (!dying)
                { anim.SetTrigger("Chase"); }
            }
            else if (minDistFromObjectReached(playerB, minDistToChase))
            { chasingTarget = playerB;
                if (!dying)
                { anim.SetTrigger("Chase"); }
                //anim.SetTrigger("Chase");
            }
        }
        else
        {
            movementDir = (chasingTarget.transform.position - transform.position);
        }
        //Die Off screen
        if (Vector2.Distance(cameraObj.transform.position, transform.position) > cameraObj.GetComponent<Camera>().orthographicSize + +12)
        { Destroy(gameObject); }
    }
    bool minDistFromObjectReached(GameObject targetObj, float minDist)
    {
        if (Vector2.Distance(transform.position, targetObj.transform.position) < minDist)
        { return true; }
        return false;
    }
   

    //Death 
    public void TriggerDeath(float timer)//called from collisions and shooting
    { StartCoroutine(StartHitAnimation(timer));
        dying = true;
    
    }
    IEnumerator StartHitAnimation(float timer)//trigger hit animation
    {
        yield return new WaitForSeconds(timer);
        anim.SetTrigger("Hit");
    }
    public void SpawnDeathParticle()//called from hit animation
    {
        GameObject newDeathEffect = Instantiate(deathEffect, transform.position, Quaternion.identity, transform.parent);
        Destroy(newDeathEffect, 4f);
    }
    public void DestroySelf()//called from hit anim
    { Destroy(gameObject); }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == cameraObj)
        { inCameraView = true; }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == cameraObj)
        {
            if (inCameraView)
            {
                inCameraView = false;
                StartCoroutine(DieOutsideOfView());
            }
        }
    }
    IEnumerator DieOutsideOfView()
    {
        while (!inCameraView)
        {
            yield return new WaitForSeconds(secondsAliveOffScreen);
            if (!inCameraView)
            { Destroy(gameObject); }
        }
    }
}
