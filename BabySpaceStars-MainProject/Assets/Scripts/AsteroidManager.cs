﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AsteroidManager : MonoBehaviour
{

    [SerializeField] GameObject enemyPrefab;
    [SerializeField] Transform enemyHolder;
    [SerializeField] [Range(.01f, .1f)] private float spawnSpeedIncreaseRate;
    [SerializeField][Range(1,5)] private float cooldown;
    [SerializeField] private bool stopSpawning;
    private float  minSpawnRadius = 9;
    private Camera cameraScript;

    void Start()
    {
        cameraScript = Camera.main;
        StartCoroutine(SpawnEnemies());
    }

    private void Update()
    { cooldown = Mathf.Clamp(cooldown -= spawnSpeedIncreaseRate * Time.deltaTime, 1, Mathf.Infinity); }

    IEnumerator SpawnEnemies()
    {
        while (!stopSpawning)//keep loopinig until manually stopped
        {
            yield return new WaitForSeconds(cooldown);//wait n(cooldown) seconds 
            //Get new random position around camera
            Vector2 randomDir = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)).normalized;
            Vector2 newSpawnPos = (Vector2)cameraScript.gameObject.transform.position + randomDir * (minSpawnRadius + cameraScript.orthographicSize);
            //Spawn new enemy
            GameObject newEnemy = Instantiate(enemyPrefab, newSpawnPos, Quaternion.identity, enemyHolder);
        }
    }
}
