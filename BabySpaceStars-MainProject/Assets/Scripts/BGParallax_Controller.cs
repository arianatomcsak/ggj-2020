﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGParallax_Controller : MonoBehaviour
{
    [SerializeField] GameObject cameraObj;
    [SerializeField] [Range(0,1)]float fraction;
    // Start is called before the first frame update
    void Awake()
    {
        cameraObj = Camera.main.transform.gameObject;
    }

    void Update()
    {
        transform.position = new Vector3(-cameraObj.transform.position.x * fraction, -cameraObj.transform.position.y * fraction, 0);
    }
}
