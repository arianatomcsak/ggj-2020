﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera_Controller : MonoBehaviour
{
    private GetCenterPoint_Component s_getCenterPoint_Component;
    private BoxCollider2D collider_Component;

    [SerializeField] private GameObject playerA, playerB;
    [SerializeField] private float lerpSpeed, zoomSpeed;
    [SerializeField] [Range(1, 10)] private int minCameraSize;
    [SerializeField] float maxCameraSize;
    [SerializeField] [Range(0, 1)] private float cameraZoomOutFraction;
    private Camera s_Camera_Component;
    private Vector2 gameSpaceBoundary = new Vector2(50,50);
    void Awake()
    {
     
        collider_Component = GetComponent<BoxCollider2D>();
        s_getCenterPoint_Component = GetComponent<GetCenterPoint_Component>();
        s_Camera_Component = GetComponent<Camera>();
    }
    private void Start()
    {
        gameSpaceBoundary = Reference_Manager.gameSpaceSize;
    }
    void Update()
    {
        //Movement
        Vector2 centerPoint = s_getCenterPoint_Component.GetCenterPoint(playerA.transform.position, playerB.transform.position);//get endPos for Lerp
        //centerPoint = new Vector2(Mathf.Clamp(centerPoint.x, -gameSpaceBoundary.x-2, gameSpaceBoundary.x+2), Mathf.Clamp(centerPoint.y, -gameSpaceBoundary.y-2, gameSpaceBoundary.y+2));
        //centerPoint = Vector2.ClampMagnitude(centerPoint, gameSpaceBoundary.magnitude/2);
        transform.position = Vector3.Lerp(transform.position, new Vector3(centerPoint.x, centerPoint.y, transform.position.z), lerpSpeed * Time.deltaTime);

        //Scale
        float distBetweenPLayers = Vector2.Distance(playerA.transform.position, playerB.transform.position);
        float lerpedCameraSize = minCameraSize + distBetweenPLayers * cameraZoomOutFraction;
        lerpedCameraSize = Mathf.Clamp(lerpedCameraSize, minCameraSize, maxCameraSize);
        s_Camera_Component.orthographicSize = Mathf.Lerp(s_Camera_Component.orthographicSize, lerpedCameraSize, zoomSpeed * Time.deltaTime);
        //collider scale
        Vector2 cameraMetrics = new Vector2( 2 * s_Camera_Component.orthographicSize * s_Camera_Component.aspect,2 * s_Camera_Component.orthographicSize);
        collider_Component.size = cameraMetrics;
    }
}
