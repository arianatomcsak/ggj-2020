﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrackPiece_Controler : MonoBehaviour//put script on moving part of crackpiece object
{
    private enum crackPieceStates { free, held, locked };
    [SerializeField] private crackPieceStates crackPieceState;

    private enum shapeTypes { a, b, c };
    [SerializeField] private shapeTypes shapeType;
    private GameObject playerInRange;
    private GameObject playerHolder = null;
    private float pickedUpTime;

    //suraj code

    private GameObject playerA, playerB;
    [SerializeField] GameObject targetPrefab;
    Transform targetTransform;
    Vector2 targetPos;
    Vector2 startPos;
    [SerializeField] float minSpeed,maxSpeed, isNearThresholdDistance, isOuterThresholdDistance;
    private float speed;
    float duration;
    float percentage, startMovingTIme;
    bool readyToMove;
    public bool runAway;
    GameObject closerPlayer;
    //values for internal use
    [SerializeField] float movementRadius;
    private GameObject currentOverllapingMatchingSpaceCrack = null;
    private bool readyToBeLocked;
    Animator anim;
    private Vector2 gameSpaceBoundary;
    private void Awake()
    {
        anim = GetComponent<Animator>();
        //get players
        playerA = Reference_Manager.playerA;
        playerB = Reference_Manager.playerB;
        gameSpaceBoundary = Reference_Manager.gameSpaceSize;
    }

    private void Start()
    {
        speed = minSpeed;
        targetTransform = Instantiate(targetPrefab, transform.position, Quaternion.identity, Reference_Manager.crackPieceTargetPositionsHolder.transform).transform;
        StartMoving();
    }

    private void Update()
    {
        if (currentOverllapingMatchingSpaceCrack != null)
        {
            if (transform.rotation.z < currentOverllapingMatchingSpaceCrack.transform.rotation.z + 10 && transform.rotation.z > currentOverllapingMatchingSpaceCrack.transform.rotation.z - 10)
            {
                readyToBeLocked = true;
            }
            else { readyToBeLocked = false; }
        }
        else { readyToBeLocked = false; }

        if (playerInRange != null && crackPieceState != crackPieceStates.locked)
        {
            Player_Controller s_PlayerController = playerInRange.GetComponent<Player_Controller>();
            if (s_PlayerController != null)
            {
                if (s_PlayerController.CrackPieceBeingHeld == null)//if player ist holding anything
                {
                    bool shootButtonDown = s_PlayerController.actionButton;
                    if (shootButtonDown)//TODO change to left trigger
                    {
                        anim.SetTrigger("Held");
                        print(playerInRange + " picked up: " + gameObject.name);
                        s_PlayerController.CrackPieceBeingHeld = gameObject;
                        playerHolder = s_PlayerController.holdingPosition;
                        crackPieceState = crackPieceStates.held;
                        pickedUpTime = Time.time;
                        readyToMove = false;
                      
                    }
                }
            }
        }   //Be picked up
        if (crackPieceState == crackPieceStates.held)
        {
            transform.position = playerHolder.transform.position;
            transform.rotation = playerHolder.transform.rotation;
            bool shootButtonDown = playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().actionButton;

            if (shootButtonDown && Time.time > pickedUpTime + .8f)
            {
                if (!readyToBeLocked)//drop to set free
                {
                    anim.SetTrigger("RunAway");
                    speed = maxSpeed;
                    playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().turnOffDecTime(true);
                    crackPieceState = crackPieceStates.free;
                    playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().CrackPieceBeingHeld = null;
                    playerHolder = null;
                    runAway = true;
                  
                    StartMoving();

                }
                else //drop in matching hole
                {
                    
                    anim.SetTrigger("LockIn");
                    crackPieceState = crackPieceStates.locked;
                    transform.position = currentOverllapingMatchingSpaceCrack.transform.position;
                    transform.rotation = currentOverllapingMatchingSpaceCrack.transform.rotation;
                    playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().CrackPieceBeingHeld = null;
                    playerHolder = null;
                }
            }
            if (crackPieceState == crackPieceStates.held)
            {
                if (playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().forceDropCrackPiece)
                {
                    anim.SetTrigger("RunAway");
                    speed = maxSpeed;
                    playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().forceDropCrackPiece = false;
                    playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().turnOffDecTime(true);
                    crackPieceState = crackPieceStates.free;
                    playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().CrackPieceBeingHeld = null;
                    playerHolder = null;
                    runAway = true;
                    StartMoving();
                }
            }
        }//be dropped
        if (crackPieceState == crackPieceStates.free)
        {
            //Get closest player
            if (Vector2.Distance(transform.position, playerA.transform.position) < Vector2.Distance(transform.position, playerB.transform.position))
            { closerPlayer = playerA; }
            else
            { closerPlayer = playerB; }

            if (Vector2.Distance(closerPlayer.transform.position, transform.position) < isNearThresholdDistance && !runAway)
            {
                runAway = true;
                StartMoving();
            }

            if (Vector2.Distance(closerPlayer.transform.position, transform.position) > isOuterThresholdDistance && runAway)
            {
                runAway = false;
                StartMoving();
            }

            if (readyToMove)
            {
                percentage = (Time.time - startMovingTIme) / duration;

                transform.position = Vector2.Lerp(startPos, targetPos, /*moveCurve.Evaluate*/(percentage));
                if (percentage >= 1)
                {
                    readyToMove = false;
                    percentage = 0;

                    StartMoving();
                }
            }
            if (Input.GetKeyDown(KeyCode.Space))
            {
                StartMoving();
            }
        }//be free
        if (speed >minSpeed)
        {
            speed -= 1 * Time.deltaTime;
        }
        else { speed = minSpeed; }
    }
    void StartMoving()
    {
        startMovingTIme = Time.time;
        readyToMove = true;
        startPos = transform.position;
        if (!runAway)//move randomly
        {
            Vector2 newDir = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
            targetPos = (Vector2)transform.position + (newDir.normalized * movementRadius);
        }
        else//run away
        {
            Vector2 newDir = ((Vector2)closerPlayer.transform.position - (Vector2)transform.position).normalized;
            targetPos = (Vector2)transform.position - newDir * .1f;
        }
        //targetPos = new Vector2(Mathf.Clamp(targetPos.x, -gameSpaceBoundary.x, gameSpaceBoundary.x), Mathf.Clamp(targetPos.y, -gameSpaceBoundary.y, gameSpaceBoundary.y));
        targetPos= Vector2.ClampMagnitude(targetPos, gameSpaceBoundary.magnitude / 2.2f);
        duration = Vector2.Distance(startPos, targetPos) / speed;
        percentage = 0;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("SpaceCrack"))
        {
            if (crackPieceState == crackPieceStates.held)
            {
                SpaceCrack_Controller s_SpaceCrack_Controller = collision.gameObject.GetComponent<SpaceCrack_Controller>();
                if (s_SpaceCrack_Controller != null)
                {
                    if (s_SpaceCrack_Controller.ShapeType() == (int)shapeType)
                    {
                        playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().turnOffDecTime(false);
                        s_SpaceCrack_Controller.OverllappingWithMatchingShape(true);
                        currentOverllapingMatchingSpaceCrack = collision.gameObject;
                    }
                }
            }
        }//Enter Hole Trigger
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (crackPieceState == crackPieceStates.free)
            {
                if (crackPieceState == 0)//if piece is in free state
                {
                    playerInRange = collision.gameObject;
                }
            }
        }//Enter Player Trigger
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("SpaceCrack"))
        {
            SpaceCrack_Controller s_SpaceCrack_Controller = collision.gameObject.GetComponent<SpaceCrack_Controller>();
            if (s_SpaceCrack_Controller != null)
            {
                if (s_SpaceCrack_Controller.ShapeType() == (int)shapeType)
                {
                    if (playerHolder != null)
                    { playerHolder.transform.parent.transform.parent.GetComponent<Player_Controller>().turnOffDecTime(true); }

                    s_SpaceCrack_Controller.OverllappingWithMatchingShape(false);
                }
            }
            currentOverllapingMatchingSpaceCrack = null;
        }//Exit Hole Trigger
        if (collision.gameObject.layer == LayerMask.NameToLayer("Player"))
        {
            if (crackPieceState == crackPieceStates.free)
            {
                if (crackPieceState == 0)//if piece is in free state
                {
                    playerInRange = null;
                }
            }
        }//Enter Player Trigger
    }
    public void DestroySpaceHole()//called from animation event
    {
        Destroy(currentOverllapingMatchingSpaceCrack);
        Reference_Manager.lockedPiecesCount += 1;
        print("locked pieces: " + Reference_Manager.lockedPiecesCount);
    }
    public void Die()
    {
        Destroy(gameObject);
    }
    


}
