﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GetCenterPoint_Component : MonoBehaviour
{

    public Vector2 GetCenterPoint(Vector2 objectA, Vector2 objectB)
    {
        float dist = Vector2.Distance(objectA, objectB);
        Vector2 dir = objectB - objectA;
        return objectA + dir.normalized * dist/2;
    }
}
