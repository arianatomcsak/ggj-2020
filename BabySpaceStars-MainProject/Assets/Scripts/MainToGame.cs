﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Rewired;

public class MainToGame : MonoBehaviour
{
    [SerializeField] GameObject BgObj;
    private Player playerA, playerB;
    private bool transitionTiggered;
    private float timeOfBirth;
    private void Awake()
    {
        timeOfBirth = Time.time;
        playerA = ReInput.players.GetPlayer(0);
        playerB = ReInput.players.GetPlayer(1);
    }
    private void Update()
    {
        if(Time.time> timeOfBirth+2f)
        if (playerA.GetButtonDown("PressButton") || playerB.GetButtonDown("PressButton"))
        {
            GameStart();
        }
    }
    public void GameStart()
    {
        if (!transitionTiggered)
        {
            transitionTiggered = true;
            BgObj.GetComponent<Animator>().SetTrigger("EndTransition");
            StartCoroutine(SwitchScene());
            GetComponent<Animator>().SetTrigger("GoAway");
        }
    }
    IEnumerator SwitchScene()
    {
        yield return new WaitForSeconds(2f);
        SceneManager.LoadScene(1);
        Destroy(gameObject);
    }
}
