﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Rewired;

public class Player_Controller : MonoBehaviour
{

    [SerializeField]
    private float accTime, decTime;

    private enum playerIDs { playerA, playerB };
    [SerializeField] private playerIDs playerID;

    public GameObject CrackPieceBeingHeld = null;
    public GameObject holdingPosition;

    public float movSpeed;
    [SerializeField] float maxMovSpeed;
    private Vector2 latestMovDir;
    public GameObject spritesHolder;
    [SerializeField] private SpriteRenderer bodySpriteRenderer, bodyPaperSpriteRenderer;
    [SerializeField] Sprite idleSprite,readyToHoldSprite, holdingSprite, shootingSprite, idlePaper, readyToHoldPaper, holdingPaper, shootingPaper;
    public float spriteAutoRotSpeed;
    private Animator anim;
    public Vector2 leftStick, rightStick;
    public bool actionButton;
    private float maxDecTime, minDecTime;
    public bool forceDropCrackPiece;
    private Player rewiredPlayer;
    private bool leftBumper, rightBumper, leftTrigger, rightTrigger;
    private Vector2 gameSpaceBoundary = new Vector2(50, 50);
    private bool inRangeOfShape;

    private void Awake()
    {
        maxDecTime = decTime;
        minDecTime = .1f;
        anim = GetComponent<Animator>();
        rewiredPlayer = playerID == playerIDs.playerA ? ReInput.players.GetPlayer(0) : ReInput.players.GetPlayer(1);

    }
    private void Start()
    {
        gameSpaceBoundary = Reference_Manager.gameSpaceSize;
    }
    public void TriggerPlayerAnimation(string trigger)
    {
        anim.SetTrigger(trigger);
    }
    public void turnOffDecTime(bool stateSwitch)
    {
        decTime = stateSwitch ? maxDecTime : minDecTime;
    }
    void Update()
    {
        leftBumper = rewiredPlayer.GetButtonDown("LeftBumper");
        rightBumper = rewiredPlayer.GetButtonDown("RightBumper");
        leftTrigger = rewiredPlayer.GetButtonDown("LeftTrigger");
        rightTrigger = rewiredPlayer.GetButtonDown("RightTrigger");
        leftStick = new Vector2(rewiredPlayer.GetAxis("MoveHorizontal"), rewiredPlayer.GetAxis("MoveVertical"));
        rightStick = new Vector2(rewiredPlayer.GetAxis("RotateHorizontal"), rewiredPlayer.GetAxis("RotateVertical"));
        //actionButton = rewiredPlayer.GetButtonDown("LeftBumper");
        if (rightTrigger || leftTrigger || leftBumper || rightBumper)
        {
            actionButton = true;
        }
        else { actionButton = false; }

        if (CrackPieceBeingHeld == null && decTime != maxDecTime)
        {
            decTime = maxDecTime;
        }


        if (CrackPieceBeingHeld != null)
        {
            if (bodySpriteRenderer.sprite != holdingSprite)
            {
                bodySpriteRenderer.sprite = holdingSprite;
                bodyPaperSpriteRenderer.sprite = holdingPaper;
            }
        }
        else if (rightStick.magnitude != 0)
        {
            if (bodySpriteRenderer.sprite != shootingSprite)
            {
                bodySpriteRenderer.sprite = shootingSprite;
                bodyPaperSpriteRenderer.sprite = shootingPaper;
            }
        } else if (inRangeOfShape)
        {
            if (bodySpriteRenderer.sprite != readyToHoldSprite)
            {
                bodySpriteRenderer.sprite = readyToHoldSprite;
                bodyPaperSpriteRenderer.sprite = readyToHoldPaper;
            }
        }
        else
        {
            if (bodySpriteRenderer.sprite != idleSprite)
            {
                bodySpriteRenderer.sprite = idleSprite;
                bodyPaperSpriteRenderer.sprite = idlePaper;
            }
        }




        //movement
        if (leftStick.magnitude >.55f)
        { //accelerate
            movSpeed = Mathf.Clamp(movSpeed + (maxMovSpeed / accTime * Time.deltaTime), 0, maxMovSpeed);
          
            latestMovDir = leftStick; ;
        }
        else if (movSpeed > 0)//decelerate
        { movSpeed -= maxMovSpeed / decTime * Time.deltaTime;}
        else { movSpeed = 0;}
        //apply movement

        transform.Translate(latestMovDir.normalized * movSpeed * Time.deltaTime, Space.World);

        //Clamp position to camera
        Vector2 maxMagnitude = Camera.main.ViewportToWorldPoint(Camera.main.rect.max);
        Vector2 minMagnitude = Camera.main.ViewportToWorldPoint(Camera.main.rect.min);
        float finalX = Mathf.Clamp(transform.position.x, minMagnitude.x + .8f, maxMagnitude.x - .8f);
        float finalY = Mathf.Clamp(transform.position.y, minMagnitude.y + .8f, maxMagnitude.y - .8f);
        transform.position = new Vector3(finalX, finalY, 0);
        transform.position = Vector2.ClampMagnitude(transform.position, gameSpaceBoundary.magnitude / 2);

        //Stick rotation
        if (rightStick.magnitude != 0)
        {

            Vector2 dir = rightStick;
            spritesHolder.transform.right = dir.normalized;
        }
        else
        {

            float rotDIr = -Mathf.Sign(latestMovDir.x);
            if (leftStick.magnitude != 0)
            {
                spriteAutoRotSpeed = rotDIr * movSpeed * 4;
            }
            else
            {
                if (Mathf.Abs(spriteAutoRotSpeed) > .1f)
                {
                    spriteAutoRotSpeed -= rotDIr * .004f;
                }
                else { spriteAutoRotSpeed = 0; }

            }
            spritesHolder.transform.Rotate(new Vector3(0, 0, spriteAutoRotSpeed * Time.deltaTime));
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Asteroid"))
        {
            collision.gameObject.GetComponent<Asteroid>().TriggerDeath(0f);
       
            GetHitAndDropShape();
        }
        if (collision.gameObject.layer == LayerMask.NameToLayer("Shape"))
        {
            inRangeOfShape = true;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.layer == LayerMask.NameToLayer("Shape"))
        {
            inRangeOfShape = false;
        }
    }
    public void GetHitAndDropShape()
    {
        anim.SetTrigger("Hit");
        if (CrackPieceBeingHeld != null)//if holding a piece
        { forceDropCrackPiece = true; }  //force drop crackpiece
    }
}
