﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Reference_Manager : MonoBehaviour
{
    [SerializeField] Vector2 gameSpaceSizeRef;
    public static int lockedPiecesCount;
    [SerializeField] private GameObject playerARef, playerBRef, crackPieceTargetPositionsHolderRef, enemiesHolderRef, BgObjRef;
    [SerializeField] private GameObject minPosRef, maxPosRef;
    public static GameObject playerA, playerB, crackPieceTargetPositionsHolder, enemiesHolder, BgObj;
    public static GameObject minPos, maxPos;
    public static Vector2 gameSpaceSize;
    private void Awake()
    {
        enemiesHolder = enemiesHolderRef;
        gameSpaceSize = gameSpaceSizeRef;
        lockedPiecesCount = 0;
        playerA = playerARef;
        playerB = playerBRef;
        crackPieceTargetPositionsHolder = crackPieceTargetPositionsHolderRef;
        minPos = minPosRef;
        maxPos = maxPosRef;
        BgObj = BgObjRef;
    }
    public static void newPieceLocked()
    {
        lockedPiecesCount += 1;
    }
    private void Update()
    {
        if (lockedPiecesCount >= 6)
        {
            StartCoroutine(LoadScene(2,5f));
        }
      
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            StartCoroutine(LoadScene(0,5f));
        }
    }
    IEnumerator LoadScene(int scene,float timer)
    {
        BgObj.GetComponent<Animator>().SetTrigger("EndTransition");
        yield return new WaitForSeconds(timer);
        SceneManager.LoadScene(scene);
    }
}
