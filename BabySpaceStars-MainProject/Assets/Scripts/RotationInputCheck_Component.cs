﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationInputCheck_Component : MonoBehaviour
{
    public Vector2 playerRightStick;
    private Player_Controller s_Player_Controller;
    public float lerpingAngle1, lerpingAngle2, inputAngle;
    public bool spinning;
    private void Awake()
    {
        s_Player_Controller = GetComponent<Player_Controller>();
        StartCoroutine(CheckRotation());
    }
    void Update()
    {
        playerRightStick = s_Player_Controller.rightStick;
        inputAngle = Mathf.Atan2(playerRightStick.y, playerRightStick.x) * Mathf.Rad2Deg;
        if (s_Player_Controller.rightStick.magnitude > .1f)
        {
            lerpingAngle1 = Mathf.LerpAngle(lerpingAngle1, inputAngle, 1);
            lerpingAngle2 = Mathf.LerpAngle(lerpingAngle2, inputAngle, Time.deltaTime * .1f);
            lerpingAngle2 = Mathf.Clamp(lerpingAngle2, lerpingAngle1 -2, lerpingAngle1 +2);
        }
        else { lerpingAngle2 = lerpingAngle1; }
    }
    IEnumerator CheckRotation()
    {
        while (true)
        {
            bool notSpinning = false;
            float oldRotationValue = GetRotationValue();
            if (oldRotationValue == 0) { notSpinning = true; }
            yield return new WaitForSeconds(.1f);
            float newRotationValue = GetRotationValue();
            if (oldRotationValue != newRotationValue) { notSpinning = true; }
            if (Mathf.Abs(lerpingAngle1 - lerpingAngle2) < 1.5f) { notSpinning = true; }
            spinning = !notSpinning;
        }

    }
    public float GetRotationValue()
    {
        if (s_Player_Controller.rightStick.magnitude > .55f)
        {
            if (Mathf.Abs(lerpingAngle1) > Mathf.Abs(lerpingAngle2))
            { return -1; }
            else if (Mathf.Abs(lerpingAngle1) < Mathf.Abs(lerpingAngle2))
            {   return 1;}
            else { return 0; }
        }
        else { return 0; }
    }

}
