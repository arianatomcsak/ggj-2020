﻿using UnityEngine;
using System.Collections;

public class Shoot : MonoBehaviour
{

    [SerializeField] private GameObject laserHitEffect;
    [SerializeField] ParticleSystem gunShotParticle;
    [SerializeField] GameObject gunPos, raycasterUp, raycasterDown;

    private LineRenderer line;
    Player_Controller s_Player_Controller;
    private float lastShotTime;
    [SerializeField] private float shootingCooldown;
    [SerializeField] private Material laserMiss, laserHit;
    [SerializeField] private bool friendlyFire;
    private GameObject holder;

    private void Awake()
    {
        lastShotTime = -Mathf.Infinity;
        s_Player_Controller = GetComponent<Player_Controller>();
        line = GetComponent<LineRenderer>();
        line.enabled = false;
        if (gunPos == null)
        { gunPos = transform.GetChild(0).gameObject; }
        if (raycasterUp == null)
        { raycasterUp = gunPos.transform.GetChild(0).gameObject; }
        if (raycasterDown == null)
        { raycasterDown = gunPos.transform.GetChild(1).gameObject; }
    }
    private void Start()
    {
        holder = Reference_Manager.enemiesHolder;
    }

    private void Update()
    {
        if (s_Player_Controller.CrackPieceBeingHeld == null)
        {
            if (s_Player_Controller.actionButton && Time.time > lastShotTime + shootingCooldown)
            {
                if (s_Player_Controller.rightStick.magnitude != 0)
                {
                    ShootRay();
                    gunShotParticle.Play();
                }
            }
            if (line.enabled)
            {
                line.SetPosition(0, new Vector3(gunPos.transform.position.x, gunPos.transform.position.y, -3));

                //Vector2 newLinePos = Vector3.Lerp(line.GetPosition(0), line.GetPosition(1), Time.deltaTime * 1f);
                line.widthMultiplier -= 6 * Time.deltaTime;
                //line.SetPosition(0, newLinePos);
            }
        }
        else
        {
            lastShotTime = Time.time;
        }
    }


    void ShootRay()
    {
        WordSpawner.InstatiatShootingWord(gunPos.transform.position+gunPos.transform.right*.4f, new Vector2(Random.Range(-.3f, .3f), Random.Range(-.3f, .3f)));
        lastShotTime = Time.time;
        line.widthMultiplier = 1;
        line.enabled = true;

        //centerRay;
        RaycastHit2D centerRay = friendlyFire ?
             Physics2D.Raycast(gunPos.transform.position, gunPos.transform.right, 200, LayerMask.GetMask("Asteroid") + LayerMask.GetMask("Player")) :
             Physics2D.Raycast(gunPos.transform.position, gunPos.transform.right, 200, LayerMask.GetMask("Asteroid"));
        if (centerRay)
        {
            line.material = laserHit;
            line.SetPosition(1, new Vector3(centerRay.point.x, centerRay.point.y, -3));
            GameObject newLaserHitEffect = Instantiate(laserHitEffect, line.GetPosition(1), Quaternion.identity);
            Destroy(newLaserHitEffect, 3f);
            if (centerRay.collider.gameObject.layer == LayerMask.NameToLayer("Asteroid"))//hit enemy
            { 
                centerRay.collider.GetComponent<Asteroid>().TriggerDeath(0f);
                WordSpawner.InstatiateEnemyHitWord(gunPos.transform.position+gunPos.transform.right*(centerRay.distance-.5f), new Vector2(Random.Range(-.3f, .3f), Random.Range(-.3f, .3f)));
            }
            else if (centerRay.collider.gameObject.layer == LayerMask.NameToLayer("Player"))//hit player
            { 
                centerRay.collider.gameObject.GetComponent<Player_Controller>().GetHitAndDropShape();
                WordSpawner.InstatiatePlayerHitByPlayerWord(gunPos.transform.position + gunPos.transform.right * (centerRay.distance - 1f), new Vector2(Random.Range(-.3f, .3f), Random.Range(-.3f, .3f)));

            }
          

        }
        else
        {
            Vector2 noTargetPos = gunPos.transform.position + gunPos.transform.right * 40;
            line.material = laserMiss;
            line.SetPosition(1, new Vector3(noTargetPos.x, noTargetPos.y, -3));
        }
        StartCoroutine(turnOffRay());
    }
    IEnumerator turnOffRay()
    {
        yield return new WaitForSeconds(.3f);
        line.enabled = false;
    }

}
