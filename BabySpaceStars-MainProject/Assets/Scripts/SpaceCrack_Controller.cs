﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceCrack_Controller : MonoBehaviour
{
    private enum shapeTypes { a, b, c };
    [SerializeField] private shapeTypes shapeType;
    private Animator anim;

    private void Awake()
    {
        anim = GetComponent<Animator>();
    }

    //Share readable shape type var
    public int ShapeType()//public shape type to read
    { return (int)shapeType; }
    //Change states
    public bool OverllappingWithMatchingShape(bool newState)//called on collision with matching shape
    {
        if (newState)
        {
            anim.SetTrigger("Interact");
        }
        return newState; 
    }

}
