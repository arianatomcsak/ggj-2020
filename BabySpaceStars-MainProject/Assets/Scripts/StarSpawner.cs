﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarSpawner : MonoBehaviour
{
    [SerializeField] private GameObject crackPiecePrefab;
    [SerializeField] private Transform crackPiecesHolder;
    [SerializeField] int starRunTimeQuantity;
    [SerializeField] private float minSpawnDistFromPlayers, possibleSpawnLocsPerStar;
    [SerializeField] public List<GameObject> crackPiecesInScene;
    [SerializeField] private List<Vector2> spawnLocations;
    [SerializeField] private Vector2 SpawnAreaWidthAndheight;
    private int startSpawnX, startSpawnY;

    [SerializeField] private GameObject cameraObj;
    //[SerializeField] private int totalCrackPieceInLevel;
    //private int crackPiecesSpawnedSoFar;
    [SerializeField] private List<GameObject> crackPiecesPrefabsToSpawn;
    private void Start()
    {
        SpawnAreaWidthAndheight = Reference_Manager.gameSpaceSize;
        //Get List of spawn locations
        startSpawnX = (int)-SpawnAreaWidthAndheight.x / 2;
        startSpawnY = (int)SpawnAreaWidthAndheight.y / 2;
        for (int xLoc = startSpawnX; xLoc < SpawnAreaWidthAndheight.x / 2; xLoc++)
        {
            for (int yLoc = startSpawnY; yLoc > -SpawnAreaWidthAndheight.y / 2; yLoc--)
            {
                Vector2 newSpawnLoc = new Vector2(xLoc, yLoc);
                spawnLocations.Add(newSpawnLoc);
            }
        }

    }
    private void Update()
    {
        if (/*crackPiecesSpawnedSoFar < totalCrackPieceInLevel*/crackPiecesPrefabsToSpawn.Count != 0)//Keep spawning untill all constellation stars are in
        {
            //remove if gone 
            for (int i = 0; i < crackPiecesInScene.Count; i++)
            {
                if (crackPiecesInScene[i] == null)
                {
                    crackPiecesInScene.RemoveAt(i);
                }
            }

            //Spawn a new star
            if (crackPiecesInScene.Count < starRunTimeQuantity)
            {

                List<Vector2> spawnLocationsAwayFromThePlayer = new List<Vector2>();
                foreach (Vector2 spawn in spawnLocations)
                {
                    if (Vector2.Distance(cameraObj.transform.position, spawn) > minSpawnDistFromPlayers)//get the spawn locs furthest away from the camera
                    {
                        spawnLocationsAwayFromThePlayer.Add(spawn);
                    }
                }
                //Spawn first star in random far away spawn
                if (crackPiecesInScene.Count < 1)
                {
                    Vector2 randomFarSpawn = spawnLocationsAwayFromThePlayer[Random.Range(0, spawnLocationsAwayFromThePlayer.Count)];

                    int randomPrefabNumber = Random.Range(0, crackPiecesPrefabsToSpawn.Count);
                    GameObject selectedPrefab = crackPiecesPrefabsToSpawn[randomPrefabNumber];
                    GameObject firstNewEnemy = Instantiate(selectedPrefab, randomFarSpawn, Quaternion.identity, crackPiecesHolder);
                    crackPiecesPrefabsToSpawn.RemoveAt(randomPrefabNumber);

                    crackPiecesInScene.Add(firstNewEnemy);
                    //crackPiecesSpawnedSoFar += 1;
                }
                //Spawn another star in one of n(possible spawn locs per star) furthest spawns from other stars
                List<Vector2> selectedSpawns = new List<Vector2>();//list of spawnLocs to be selected as options
                for (int i = 0; i < possibleSpawnLocsPerStar; i++)
                {
                    Vector2 tempSelectedSpawn = Vector2.zero;
                    float LongestDist = 0;

                    for (int j = 0; j < spawnLocationsAwayFromThePlayer.Count; j++)
                    {
                        //skip iteration if spawn was picked in a previous iteration
                        bool skipThisIteration = false;
                        foreach (Vector2 pickedSpawn in selectedSpawns)
                        {
                            if (spawnLocationsAwayFromThePlayer[j] == pickedSpawn)
                            { skipThisIteration = true; }
                        }
                        //else
                        if (!skipThisIteration)
                        {
                            //Get closest distance from spawn to all stars
                            float distFromCLosestEnemy = Mathf.Infinity;
                            foreach (GameObject crackPiece in crackPiecesInScene)
                            {
                                if (crackPiece != null)
                                {
                                    float dist = Vector2.Distance(spawnLocationsAwayFromThePlayer[j], crackPiece.transform.position);
                                    if (dist < distFromCLosestEnemy)
                                    { distFromCLosestEnemy = dist; }
                                }
                            }
                            //Compare closest distance to previous saved closest dist
                            if (distFromCLosestEnemy > LongestDist)//if dist is higher than the saved distance
                            {
                                //save spawn and distance as a temp candidate for selection 
                                LongestDist = distFromCLosestEnemy;
                                tempSelectedSpawn = spawnLocationsAwayFromThePlayer[j];
                            }
                        }
                    }
                    selectedSpawns.Add(tempSelectedSpawn);//save candidate for spawn location
                }

                //spawn star
                if (crackPiecesInScene.Count < starRunTimeQuantity)
                {
                    //print(selectedSpawns.Count);
                    Vector2 selectedSpawn = selectedSpawns[Random.Range(0, selectedSpawns.Count)];//pick random spawn from candidates

                    int randomPrefabNumber = Random.Range(0, crackPiecesPrefabsToSpawn.Count);
                    GameObject selectedPrefab = crackPiecesPrefabsToSpawn[randomPrefabNumber];
                    GameObject newEnemy = Instantiate(selectedPrefab, selectedSpawn, Quaternion.identity, crackPiecesHolder);
                    crackPiecesPrefabsToSpawn.RemoveAt(randomPrefabNumber);

                    crackPiecesInScene.Add(newEnemy);
                    //crackPiecesSpawnedSoFar += 1;
                }
            }
        }
    }
}
