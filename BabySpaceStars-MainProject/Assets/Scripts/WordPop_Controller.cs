﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WordPop_Controller : MonoBehaviour
{
    Vector2 worldPos;
    void Awake()
    {
        worldPos = Camera.main.ScreenToWorldPoint(transform.position);
    }
    void Update()
    {
        transform.position = Camera.main.WorldToScreenPoint(worldPos);
    }
    public void DestroySelf()//called from animation event
    {
        Destroy(gameObject);
    }
}
