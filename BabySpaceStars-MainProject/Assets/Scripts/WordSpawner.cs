﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordSpawner : MonoBehaviour
{
    [SerializeField] private GameObject canvasObjRef, shoothingWordRef, enemyWordRef;//ref
    private static GameObject canvasObj, shootingWordsPrefab, enemyHitWordPrefab;
    [SerializeField] private List<string> shootingWordsRef, enemyHitWordsRef, playerHitByplayerWordsRef;//ref
    private static List<string> shootingWords,enemyHitWords, playerHitByplayerWords;
    private void Awake()
    {
        canvasObj = canvasObjRef;
        shootingWordsPrefab = shoothingWordRef;
        enemyHitWordPrefab = enemyWordRef;
        shootingWords = shootingWordsRef;
        enemyHitWords = enemyHitWordsRef;
        playerHitByplayerWords = playerHitByplayerWordsRef;
    }

    public static void InstatiatShootingWord(Vector2 position, Vector2 positionOffset)
    {
        InstantiateWord(position, positionOffset,shootingWordsPrefab, shootingWords[Random.Range(0, shootingWords.Count)]);
    }
    public static void InstatiateEnemyHitWord(Vector2 position, Vector2 positionOffset)
    {
        InstantiateWord(position, positionOffset, enemyHitWordPrefab, enemyHitWords[Random.Range(0, enemyHitWords.Count)]);
    }
    public static void InstatiatePlayerHitByPlayerWord(Vector2 position, Vector2 positionOffset)
    {
        InstantiateWord(position, positionOffset, enemyHitWordPrefab, playerHitByplayerWords[Random.Range(0, playerHitByplayerWords.Count)]);
    }
    private static void InstantiateWord(Vector2 position, Vector2 positionOffset,GameObject prefab,string word)
    {
        Vector2 newWordPos = Camera.main.WorldToScreenPoint(position + positionOffset);
        float randomRotation = Random.Range(-30f, 30f);
        GameObject newWord = Instantiate(prefab, newWordPos, Quaternion.Euler(0, 0, Quaternion.identity.z + randomRotation), canvasObj.transform);
        newWord.GetComponentInChildren<Text>().text = word;
    }
}
