﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    //Define public target variable to set it to player in inspector
    public Transform target;

    //Define public variable for decimal position between the camera move points
    public float smoot;

    //For Camera to not interfere with z-axis & other way around (it's consensual)
    public Vector3 offset;



    //Camera lerping from intial position to next position following target (player)
    private void LateUpdate()
    {
        Vector3 finalPosition = target.position + offset;
        Vector3 smote = Vector3.Lerp(transform.position, finalPosition, smoot);

        transform.position = smote;
    }
}
