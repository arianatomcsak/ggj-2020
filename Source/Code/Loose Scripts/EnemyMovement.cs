﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemyMovement : MonoBehaviour
{

    public AnimationCurve animationCurve;

    //Define public speed variable
    public float speed;

    //Define public distance variable 
    float distance;

    //Define variable for the start time of Enemy object
    float startTime;

    //Define variable for Start postiion
    Vector3 startPosition;



    //Setting start position to be equal to the position at which the enemy is placed at
    private void Awake()
    {
        startPosition = transform.position;

        startTime = Time.time;
    }


    //Enemy movement towards player 
    private void Update()
    {
        //Calculating distance travelled over time based on speed and the time that has passed
        float distanceTravelled = (Time.time - startTime) * speed;

        //A fraction of the distance is travelled based on the distance travelled from the total distance 
        float fractionDistance = distanceTravelled / distance;
        distance = Vector3.Distance(startPosition, PlayerMovement.playerPosition);

        //Lerp enemy position towards player
        transform.position = Vector3.Lerp(startPosition, PlayerMovement.playerPosition, animationCurve.Evaluate(distanceTravelled));
    }



    //Invoke the Boop function after 0.7 seconds when message is received 
    private void Ded()
    {
        Invoke("Boop", 0.7f);
    }



    //Load Game Over screen after Boop function is invoked
    private void Boop()
    {
        SceneManager.LoadScene("Death Screen");
    }
}


   


